variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"

}

variable "contact" {
  default = "mmoreirasantosdev@gmail.com"
}


variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}


variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "825377780233.dkr.ecr.sa-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "825377780233.dkr.ecr.sa-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

